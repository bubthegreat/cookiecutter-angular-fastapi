"""Basic import tests for {{ cookiecutter.python_slug }}."""

import pytest

def test_import():
    """Test that we can import the installed module successfully."""
    import {{ cookiecutter.python_slug }}
